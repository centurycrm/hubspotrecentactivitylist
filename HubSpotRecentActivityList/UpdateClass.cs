﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace HubSpotRecentActivityList
{
    public enum httpVerb
    {
        GET,
        POST,
        PUT,
        DELETE
    }
    class UpdateClass
    {
        public string List { get; set; }
        public string updateURL { get; set; }
        public string GetCURCRED { get; set; }
        public string GetORGCRED { get; set; }
        public string GetLSFOUR { get; set; }
        public string GetPAYPRO { get; set; }
        public string GetCancelDate { get; set; }

        public string updateRequest()
        {
            //Grabs todays date and converts it to milliseconds because thats how HS reads in dates
            var date = (DateTime.Now.Month.ToString("d2") + "/" + DateTime.Now.Day.ToString("d2") + "/" + DateTime.Now.Year.ToString("d4"));
            var datecon = Convert.ToDateTime(date);
            var today = (datecon - new DateTime(1970, 1, 1)).TotalMilliseconds;

            string strResponseValue = string.Empty;

            //Depending on what List is equal to then one of the cases will be preformed
            switch (List)
            {
                case "EFAX":
                    strResponseValue = "{\"properties\":[{\"property\": \"loatocreditor\",\"value\": \"" + today + "\"},{\"property\": \"current_creditor\",\"value\": \"" + GetCURCRED + "\"},{\"property\": \"original_creditor\",\"value\": \"" + GetORGCRED + "\"},{\"property\": \"last_4_of_account_\",\"value\": \"" + GetLSFOUR + "\"},{\"property\": \"paymentprocessor\",\"value\": \"" + GetPAYPRO + "\"}]}";
                    break;
                case "Account Marked Settled":
                    strResponseValue = "{\"properties\":[{\"property\": \"acctsettled\",\"value\": \"" + today + "\"},{\"property\": \"current_creditor\",\"value\": \"" + GetCURCRED + "\"},{\"property\": \"original_creditor\",\"value\": \"" + GetORGCRED + "\"},{\"property\": \"last_4_of_account_\",\"value\": \"" + GetLSFOUR + "\"},{\"property\": \"paymentprocessor\",\"value\": \"" + GetPAYPRO + "\"}]}";
                    break;
                case "Account Marked Proposal":
                    strResponseValue = "{\"properties\":[{\"property\": \"acctproposal\",\"value\": \"" + today + "\"},{\"property\": \"current_creditor\",\"value\": \"" + GetCURCRED + "\"},{\"property\": \"original_creditor\",\"value\": \"" + GetORGCRED + "\"},{\"property\": \"last_4_of_account_\",\"value\": \"" + GetLSFOUR + "\"},{\"property\": \"paymentprocessor\",\"value\": \"" + GetPAYPRO + "\"}]}";
                    break;
                case "Payment to Credior":
                    strResponseValue = "{\"properties\":[{\"property\": \"sifpaymentsent\",\"value\": \"" + today + "\"},{\"property\": \"current_creditor\",\"value\": \"" + GetCURCRED + "\"},{\"property\": \"original_creditor\",\"value\": \"" + GetORGCRED + "\"},{\"property\": \"last_4_of_account_\",\"value\": \"" + GetLSFOUR + "\"},{\"property\": \"paymentprocessor\",\"value\": \"" + GetPAYPRO + "\"}]}";
                    break;
                case "New Review":
                    strResponseValue = "{\"properties\":[{\"property\": \"amnewreview\",\"value\": \"" + today + "\"},{\"property\": \"paymentprocessor\",\"value\": \"" + GetPAYPRO + "\"}]}";
                    break;
                case "NATS Task":
                    strResponseValue = "{\"properties\":[{\"property\": \"natsdialerlist\",\"value\": \"" + today + "\"},{\"property\": \"paymentprocessor\",\"value\": \"" + GetPAYPRO + "\"}]}";
                    break;
                case "NCA Prequal":
                    strResponseValue = "{\"properties\":[{\"property\": \"ncaprequallist\",\"value\": \"" + today + "\"}]}";
                    break;
                case "Third Party Cancel":
                    strResponseValue = "{\"properties\":[{\"property\": \"thirdpartycancels\",\"value\": \"" + today + "\"},{\"property\": \"third_party_bank_name\",\"value\": \"" + GetPAYPRO + "\"}]}";
                    break;
                case "Cancel":
                    strResponseValue = "{\"properties\":[{\"property\": \"cancel\",\"value\": \"YES\"},{\"property\": \"cancel_date\",\"value\": \"" + GetCancelDate + "\"}]}";
                    //strResponseValue = "{\"properties\":[{\"property\": \"cancel\",\"value\": \"YES\"}]}";
                    break;
                default:
                    strResponseValue = "null";
                    break;
            }

            HttpWebRequest request3 = (HttpWebRequest)WebRequest.Create(updateURL);
            request3.ContentType = "application/json; charset=utf-8";
            request3.Method = "POST";
            request3.Accept = "application/json; charset=utf-8";

            using (var streamWriter = new StreamWriter(request3.GetRequestStream()))
            {
                streamWriter.Write(strResponseValue);
            }
            try
            {
                using (HttpWebResponse response = (HttpWebResponse)request3.GetResponse())
                {
                    if (response.StatusCode != HttpStatusCode.OK)
                    {
                        try
                        {
                            throw new ApplicationException("error code:" + response.StatusCode.ToString());
                        }
                        catch (ApplicationException et)
                        {
                            //throw new ApplicationException("error code:");
                        }
                    }
                    //Process the response stream... (could be JSON, XML or HTML ect...)

                    using (Stream responseStream = response.GetResponseStream())
                    {
                        if (responseStream != null)
                        {
                            using (StreamReader reader = new StreamReader(responseStream))
                            {
                                strResponseValue = reader.ReadToEnd();
                            }
                        }
                    }
                }
            }
            catch (WebException e)
            {
                if (e.Status == WebExceptionStatus.ProtocolError)
                {
                    WebResponse resp = e.Response;
                    using (StreamReader sr = new StreamReader(resp.GetResponseStream()))
                    {
                        strResponseValue = sr.ReadToEnd();
                    }
                }
            }

            return strResponseValue;
        }
    }
}
