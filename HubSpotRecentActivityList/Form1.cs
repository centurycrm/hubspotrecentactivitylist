﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.OleDb;
using System.Data.SqlClient;
using System.Net.Mail;
using System.Text;
using System.Windows.Forms;
using static HubSpotRecentActivityList.Program;
using Outlook = Microsoft.Office.Interop.Outlook;
using System.Security.Cryptography.X509Certificates;
using System.Net.Security;
using System.Net;

namespace HubSpotRecentActivityList
{
    public partial class Form1 : Form
    {
        private string ACkact = "";
        public Form1()
        {
            InitializeComponent();
        }

        public SqlConnection sql_connection = new SqlConnection("Data Source=SQL03;Initial Catalog=CNI;User id=WEB2;Password=Emdweb#1;");
        public string string_connection = "PROVIDER=SQLOLEDB;Data Source=SQL03;Initial Catalog=CNI;UID=WEB2;PWD=Emdweb#1;";
        public OleDbDataAdapter OleDbDataAdapter;
        public string sql_query;
        public DataSet dataset1;
        public OleDbConnection OleDbConnection;

        private void btnUpdateFields_Click(object sender, EventArgs e)
        {
            UpdateClass connection = new UpdateClass();

            try
            {
                //Create file name for output
                Dictionary<string, int> dictionary = new Dictionary<string, int>();

                var sb = new StringBuilder();

                string vDate, sList, vDate2;

                var date = (DateTime.Now.Month.ToString() + "/" + DateTime.Now.Day.ToString("d2") + "/" + DateTime.Now.Year.ToString("d4"));
                var datecon = Convert.ToDateTime(date);

                //Day of week to pull the list from
                //production
                if (DateTime.Now.DayOfWeek == DayOfWeek.Monday)
                { vDate = DateTime.Now.AddDays(-3).ToShortDateString(); }
                else { vDate = DateTime.Now.AddDays(-1).ToShortDateString(); }

                sList = "EFAX";
                connection.List = sList;

                var connetionString = ConfigurationManager.ConnectionStrings["Application_DatabaseConnection"].ConnectionString;
                SqlConnection sqlConnection = new SqlConnection(connetionString);

                DataTable sqlDt = new DataTable();

                string sqlSelect = "select CC.FriendlyId as CLTID, " +
                          "CE.Email as EMAIL, CN.FirstName as FNAME, CN.LastName as LNAME, " +
                          "L.Id as CREDPK, PID.id as CKACT, " +
                          "LLL.name as [Current Creditor],LLL.OriginalCreditor as [Original Creditor],  RIGHT(LL.AccountNumber, 4) AS[Last 4] " +
                          "from loan.Loan L " +
                          "join loan.LoanLender as LL on LL.loanId = L.id " +
                          "join loan.Lender as LLL on LLL.id = LL.LenderId " +
                          "join client.Contract as CC on cc.Id = L.ContractId " +
                          "join client.contractapplicant as cap on cap.contractid = CC.id " +
                          "join Client.Name as CN on CN.ClientId = cap.clientid " +
                          "join Client.Email as CE ON CE.clientid = cap.clientid " +
                          "join [audit].UserContractChange UCC on UCC.contractid = CC.Id " +
                          "join[audit].UserContractChangeNote UCN on UCN.UserContractChangeId = UCC.Id " +
                          "left outer join client.ContractPaymentIntegration as CPI on CPI.ContractId = CC.id " +
                          "left outer join payment.PaymentIntegrationDefinition as PID on PID.Id = CPI.PaymentIntegrationId " +
                          "left outer join client.CancelDetails ccd on ccd.ContractId = CC.id " +
                          "where UCN.Notes LIKE '%Efax:%' " +
                          "and UCC.TimeStamp Between '" + vDate + "' And '" + DateTime.Now.ToShortDateString() + "' " +
                          "AND CE.Email LIKE '%@%.%' and ccd.Id is null" +
                              " AND left(CC.FriendlyId,3) > 800 and left(CC.FriendlyId,3) < 899 ";

                SqlCommand sqlCommand = new SqlCommand(sqlSelect, sqlConnection);
                SqlDataAdapter sqlDa = new SqlDataAdapter(sqlCommand);
                sqlDa.Fill(sqlDt);
                if(sqlDt.Rows.Count > 0)
                {
                    foreach (DataRow row in sqlDt.Rows[0].Table.Rows) // gvs.DS1.Tables[0].Rows
                    {
                        string CLTID = row["CLTID"].ToString();
                        switch (Convert.ToInt32(ACkact = row["CKACT"].ToString()))
                        {
                            case 20:
                            case 21:
                                ACkact = " Reliant Account Management (RAM)";
                                //document.Bookmarks["bbank2"].Range.Text = "RAM";
                                break;
                            case 7:
                            case 71:
                                ACkact = "Crossroads Financial Technologies (CFT)";
                                //document.Bookmarks["bbank2"].Range.Text = "CFT";
                                break;
                            default:
                                ACkact = "Global Client Solutions (GCS) ";
                                //document.Bookmarks["bbank2"].Range.Text = "GCS";
                                break;
                        }

                        if (!dictionary.ContainsKey(row["CLTID"].ToString()))
                        {
                            string CURCRED = row["Current Creditor"].ToString();
                            string ORGCRED = row["Original Creditor"].ToString();
                            string LSFOUR = row["Last 4"].ToString();

                            dictionary.Add(row["CLTID"].ToString(), Convert.ToInt32(row["CLTID"]));



                            string sqlUpdate1 = "UPDATE hubspot.HSCLIENT SET LOATOCREDITOR = '" + date + "', CURCRED = '" + CURCRED + "', ORGCRED = '" + ORGCRED + "', LASTFOUR = '" + LSFOUR + "', PAYPRO = '" + ACkact + "' " +
                            "WHERE CLTID = '" + row["CLTID"] + "' AND HSEMAIL = '" + row["EMAIL"] + "'";

                            sqlConnection = new SqlConnection(connetionString);
                            sqlConnection.Open();
                            SqlCommand myCommand = new SqlCommand(sqlUpdate1, sqlConnection);
                            myCommand.ExecuteNonQuery();
                            sqlConnection.Close();
                        }
                    }
                }
                

                DataTable sqlDt0 = new DataTable();
                string sqlSelect0 = "SELECT HC.CLTID, HC.HSVID, HC.LOATOCREDITOR, HC.CURCRED, HC.ORGCRED, HC.LASTFOUR, HC.PAYPRO, CC.Reference as Reference " +
                                  "FROM hubspot.HSCLIENT as HC join client.contract  CC on CC.FriendlyId= HC.CLTID " +
                                  //production
                                  "WHERE  FORMAT(HC.LOATOCREDITOR, 'd', 'en-us')  = '" + (Convert.ToDateTime(date)).ToShortDateString() + "'" +
                              " AND left(CC.FriendlyId,3) > 800 and left(CC.FriendlyId,3) < 899 ";
                sqlConnection = new SqlConnection(connetionString);

                SqlCommand sqlCommand0 = new SqlCommand(sqlSelect0, sqlConnection);
                SqlDataAdapter sqlDa0 = new SqlDataAdapter(sqlCommand0);
                sqlDa0.Fill(sqlDt0);

                if(sqlDt0.Rows.Count > 0)
                {
                    foreach (DataRow row in sqlDt0.Rows[0].Table.Rows)
                    {
                        string sHSVID = row["HSVID"].ToString();
                        string CURCRED = row["CURCRED"].ToString();
                        string ORGCRED = row["ORGCRED"].ToString();
                        string LSFOUR = row["LASTFOUR"].ToString();
                        string PAYPRO = row["PAYPRO"].ToString();

                        string url = string.Format("https://api.hubapi.com/contacts/v1/contact/vid/" + sHSVID + "/profile?hapikey=27223930-be9c-4e1e-b60f-0c59d42c3b10&property=loatocreditor");

                        connection.updateURL = url;

                        connection.GetCURCRED = CURCRED;
                        connection.GetORGCRED = ORGCRED;
                        connection.GetLSFOUR = LSFOUR;
                        connection.GetPAYPRO = PAYPRO;

                        string strResponse3 = string.Empty;

                        strResponse3 = connection.updateRequest();

                        //string vSQL = "INSERT INTO NOTES (NTCLTID,NTTYP,NTDTE,NTREAS,NTWHO,NTNOTE) VALUES (" + row["CLTID"] + ",'Hubspot','" + DateTime.Now.ToString() + "','Recent Activity EFAX'" + ",'Hubspot'" + ",'Century LCM Recent Activity email sent to client regarding LOA sent to creditor')";
                        //gvs.conn1.Open();
                        //SqlCommand cmd = new SqlCommand(vSQL, gvs.conn1);
                        //cmd.ExecuteNonQuery();
                        //gvs.conn1.Close();

                        var clientNotes = "Century LCM Recent Activity email sent to client regarding LOA sent to creditor";
                        sqlConnection = new SqlConnection(connetionString);
                        InsertClientNotes(Convert.ToInt32(row["CLTID"]), row["Reference"].ToString(), clientNotes, sqlConnection);
                    }
                }

            }
            catch (Exception ex)
            {
                Outlook.Application oApp = new Outlook.Application();
                Outlook.MailItem oMsg = (Outlook.MailItem)oApp.CreateItem(Outlook.OlItemType.olMailItem);

                gvs.sErrorMsg += ex + Environment.NewLine;

                Outlook.Recipient oRecip = oMsg.Recipients.Add("mkirchner@Centuryss.com");
                oRecip = oMsg.Recipients.Add("BVasquez@CenturySS.com");
                oRecip.Resolve();
                oMsg.Subject = "HS Recent Activity Errors " + DateTime.Now.Date;

                string sEB = "All," + Environment.NewLine + Environment.NewLine;
                sEB += gvs.sErrorMsg + Environment.NewLine + Environment.NewLine;
                sEB += "Thanks";
                //oMsg.Attachments.Add(@"\\fs12\database\IT\Brad D\HubSpot Daily\CancelledClients" + DateTime.Now.Month + DateTime.Now.Day + DateTime.Now.Year + ".csv");
                oMsg.Body = sEB;
                oMsg.Save();
                oMsg.Send();

                oRecip = null;
                oMsg = null;
                oApp = null;
            }

            Environment.Exit(0);
        }

        public void EFAX()
        {

        }

        public void sql_update(string sql_update_query)
        {
            try
            {
                sql_connection.Open();
                SqlCommand sql_command = new SqlCommand(sql_update_query, sql_connection);
                sql_command.ExecuteNonQuery();
                sql_connection.Close();
            }
            catch (Exception error)
            {
                Outlook.Application oApp = new Outlook.Application();
                Outlook.MailItem oMsg = (Outlook.MailItem)oApp.CreateItem(Outlook.OlItemType.olMailItem);

                gvs.sErrorMsg += error + Environment.NewLine;

                Outlook.Recipient oRecip = oMsg.Recipients.Add("mkirchner@Centuryss.com");
                oRecip = oMsg.Recipients.Add("BVasquez@CenturySS.com");
                oRecip.Resolve();
                oMsg.Subject = "HubSpot Recent Activity SQL Update Error " + DateTime.Now.Date;
                //oMsg.Attachments.Add(@"\\fs12\database\IT\Brad D\HubSpot Daily\CancelledClients" + DateTime.Now.Month + DateTime.Now.Day + DateTime.Now.Year + ".csv");
                oMsg.HTMLBody = "<HTML><BODY></BR><P>" + error + "</P></BR></BODY></HTML> ";
                oMsg.Save();
                oMsg.Send();

                oRecip = null;
                oMsg = null;
                oApp = null;
            }
        }

        public void sql_insert(string sql_insert_query)
        {
            try
            {
                sql_connection.Open();
                SqlCommand sql_command = new SqlCommand(sql_insert_query, sql_connection);
                sql_command.ExecuteNonQuery();
                sql_connection.Close();
            }
            catch (Exception error)
            {
                Outlook.Application oApp = new Outlook.Application();
                Outlook.MailItem oMsg = (Outlook.MailItem)oApp.CreateItem(Outlook.OlItemType.olMailItem);

                gvs.sErrorMsg += error + Environment.NewLine;

                Outlook.Recipient oRecip = oMsg.Recipients.Add("mkirchner@Centuryss.com");
                oRecip = oMsg.Recipients.Add("BVasquez@CenturySS.com");
                oRecip.Resolve();
                oMsg.Subject = "HubSpot Recent Activity SQL Insert Error " + DateTime.Now.Date;
                //oMsg.Attachments.Add(@"\\fs12\database\IT\Brad D\HubSpot Daily\CancelledClients" + DateTime.Now.Month + DateTime.Now.Day + DateTime.Now.Year + ".csv");
                oMsg.HTMLBody = "<HTML><BODY></BR><P>" + error + "</P></BR></BODY></HTML> ";
                oMsg.Save();
                oMsg.Send();

                oRecip = null;
                oMsg = null;
                oApp = null;
            }
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            ServicePointManager.ServerCertificateValidationCallback =
             delegate (object s, X509Certificate certificate,
                      X509Chain chain, SslPolicyErrors sslPolicyErrors)
             { return true; };
            System.Net.ServicePointManager.SecurityProtocol = System.Net.SecurityProtocolType.Tls12;

            btnUpdateFields.PerformClick();
        }


        public static void InsertClientNotes(int contractId, string reference, string notes, SqlConnection sql_connection)
        {
            using (SqlCommand cmd = new SqlCommand("[audit].[AuditContractChange]", sql_connection))
            {
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@userId", 1);
                cmd.Parameters.AddWithValue("@contractId", null);
                cmd.Parameters.AddWithValue("@reference", reference);
                cmd.Parameters.AddWithValue("@changeTypeId", 7);
                cmd.Parameters.AddWithValue("@notes", notes);

                DataTable fieldDetails = CreateUserContractChangeDetailTable();
                cmd.Parameters.AddWithValue("@fieldDetails", fieldDetails);
                cmd.Parameters.AddWithValue("@loanId", null);
                sql_connection.Open();
                cmd.ExecuteNonQuery();
                sql_connection.Close();
            }
        }
        public static DataTable CreateUserContractChangeDetailTable()
        {
            DataTable dt = new DataTable();
            dt.Columns.Add("FieldId", typeof(Byte));
            dt.Columns.Add("OldValue", typeof(string));
            dt.Columns.Add("NewValue", typeof(string));
            return dt;
        }
    }
}
